<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\General;
use App\Models\Upload;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\UploadRequest;

use Storage;

class UploadController extends Controller
{
    
    public $model;
    public function __construct(Upload $model)
    {
        $this->model = $model;
    }

    public function upload(UploadRequest $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            $input['note'] = $this->searchSameCharacter($input['username']);

            $image = $request->file('image');
            $image_name = Storage::putFile('image/', $image, 'public');

            $input['image'] = $image_name;
            $data = $this->model->create($input);

            DB::commit();

            $messages = [
                'code'      => 200,
                'message'   => "successfully",
                'data'      => $data
            ];
        } catch (\Exception $e) {
            $messages = [
                'code'      => 500,
                'message'   => $e->getMessage()
            ];

            DB::rollback();
        }

        return response()
            ->json($messages)
            ->header('Content-Type', 'application/json');
    }

    function searchSameCharacter($str)
    {
        foreach (count_chars($str, 1) as $key => $val)
            if ($val >= 1)
                return false;
        
        return true;
    }
}
